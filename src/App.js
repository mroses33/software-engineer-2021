import Input from './Components/Input';
import Table from './Components/Table';
import Header from './Components/Header'
import React, {useState} from 'react';

const App = () => {
  const [employeeData, setEmployeeData] = useState([{first: "", last: "", email: "", delete:""}]);

  return (
    <div>
      <Header />
      <Input employeeData={employeeData} setEmployeeData={setEmployeeData}/>
      <Table employeeData={employeeData} setEmployeeData={setEmployeeData}/>
    </div>
  );
}

export default App;
