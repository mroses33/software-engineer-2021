import React, {useState} from 'react';



const InputField = ({employeeData, setEmployeeData}) => {

  const clearForm = () => {
    document.querySelector(".form-inline").reset();
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    let newEmployee = [{
      first: e.currentTarget.name.value,
      last: e.currentTarget.lastname.value,
      email: e.currentTarget.email.value,
      delete: "➖"
    }];
    setEmployeeData(employeeData.concat(newEmployee))
    clearForm();
  }


  return (
      <div>
        <form className="form-inline" onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="firstname">First Name:</label>
            <input type="text" className="form-control" id="name" placeholder="First Name"
            value={employeeData.first} pattern="^[a-zA-Z][a-zA-Z-]+" title="Must be a valid name" required/>
          </div>
          <div className="form-group">
            <label htmlFor="lastname">Last Name: </label>
            <input type="text" className="form-control" id="lastname" placeholder="Last name"
            value={employeeData.last} pattern="^[a-zA-Z][a-zA-Z-]+" title="Must be a valid name" required/>
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input type="email" className="form-control" id="email" name="email"placeholder="E-mail"
            value={employeeData.email} placeholder="E-mail"/>
          </div>
          <button type="submit" className="btn btn-primary">Add</button>
        </form>
      </div>

  )
};

export default InputField;
