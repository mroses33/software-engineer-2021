import React, {useState} from 'react';

const Table = ({employeeData, setEmployeeData}) => {

    const handleDelete = (e) => {
      const confirmDelete = window.confirm("Are you sure you want to delete this employee?");
      if(confirmDelete) {
        const rowIndex = e.currentTarget.parentElement.rowIndex - 1;
        setEmployeeData(employeeData.filter(obj => employeeData[rowIndex] !== obj));
      }
    }

    return (
        <div>
          <table className="name-table">
             <thead>
               <tr>
                 <th>Last Name</th>
                 <th>First Name</th>
                 <th>Email</th>
                 <th>Delete?</th>
               </tr>
             </thead>
             <tbody>
              {employeeData.sort((a, b) => a.last.localeCompare(b.last))
                .map((name, index) => (
                 <tr key={index}>
                   <td>{name.last}</td>
                   <td>{name.first}</td>
                   <td>{name.email}</td>
                   <td onClick={handleDelete}>{name.delete}</td>
                 </tr>
                ))}
             </tbody>
          </table>
        </div>

    )
};

export default Table;
